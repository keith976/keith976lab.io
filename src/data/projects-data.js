const ProjectsData = [
  {
    project_title: 'Headless Commerce Accelerator',
    project_company: 'EPAM Systems',
    project_type: 'E-Commerce',
    project_status: 'Live',
    project_image: 'HCA/project.png',
    project_path: '/hca',
    project_stack: [
      ['language', 'library'],
      ['typescript', 'react js']
    ],
    project_information: [
      ['Development Time'],
      ['1 Year'],
    ],
    data: [
      'This is a descriptive text.'
    ],
  },
  {
    project_title: 'Personal Portfolio',
    project_company: 'Keith Eng',
    project_type: 'Services',
    project_image: 'portfolio/portfolio.png',
    project_path: '/portfolio',
    project_status: 'Live',
    project_stack: [
      [],
      []
    ],
    project_information: [
      [],
      []
    ],
    data: [
      'This is a descriptive text.'
    ],
  },
];

export default ProjectsData;