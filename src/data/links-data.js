const LinksData = [
  {
    header: 'Work',
    data: [
      {
        icon_name: 'social-media/svg/company.svg',
        subheader: 'Company',
        content: 'EPAM',
        link: 'https://epam.com', 
      },
      {
        icon_name: 'social-media/svg/id-card.svg',
        subheader: 'Title',
        content: 'Software Engineer',
      },
    ],
  },
  {
    header: 'Business',
    data: [
      {
        icon_name: 'social-media/svg/002-linkedin.svg',
        subheader: 'LinkedIn',
        content: 'ENG KEITH',
        link: 'https://linkedin.com/in/engkeith/', 
      },
      {
        icon_name: 'social-media/svg/gitlab-icon.svg',
        subheader: 'GitLab',
        content: 'Keith976/Projects',
        link: 'https://gitlab.com/users/keith976/projects', 
      },
      {
        icon_name: 'social-media/svg/github.svg',
        subheader: 'GitHub',
        content: 'Keith976',
        link: 'https://github.com/keith976', 
      },
    ],
  },
  {
    header: 'socials',
    data: [
      {
        icon_name: 'social-media/svg/049-facebook.svg',
        subheader: 'Facebook',
        content: 'Keith Eng',
        link: 'https://www.facebook.com/keithpotato/', 
      },
      {
        icon_name: 'social-media/svg/060-instagram.svg',
        subheader: 'Instagram',
        content: '@iamnotimpossiblebeefburger',
        link: 'https://www.instagram.com/iamnotimpossiblebeefburger/?hl=en', 
      },
      {
        icon_name: 'social-media/svg/057-spotify.svg',
        subheader: 'Spotify',
        content: 'Keith976',
        link: 'https://open.spotify.com/user/keith976', 
      },
    ],
  }
];

export default LinksData;