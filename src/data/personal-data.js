const Personals = {
  image_path: 'me.jpg',
  name: 'Eng Zhen Xuan, Keith',
  bio: 'Avid technosociologist. Aspiring chef. Innovatorist.',
  email: 'keith976@hotmail.com',
  phone: '+65-94512273',
};

export default Personals;