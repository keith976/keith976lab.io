import React from 'react';
import Breakpoint from './Breakpoint';

const Tablet = (props) => {
  return (
    <Breakpoint name="tablet">
      {props.children}
    </Breakpoint>
  );
}
export default Tablet;