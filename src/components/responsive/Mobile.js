import React from 'react';
import Breakpoint from './Breakpoint';

const Mobile = (props) => {
  return (
    <Breakpoint name="mobile">
      {props.children}
    </Breakpoint>
  );
}
export default Mobile;