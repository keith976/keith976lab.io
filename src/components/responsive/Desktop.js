import React from 'react';
import Breakpoint from './Breakpoint';

const Desktop = (props) => {
  return (
    <Breakpoint name="desktop">
      {props.children}
    </Breakpoint>
  );
}
export default Desktop;