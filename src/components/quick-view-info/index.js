import React from 'react';
import LinksContainer from './links/links-container';

//Data Set
import LinksData from '../../data/links-data';
import Personals from '../../data/personal-data';

//Styles
import './index.scss';

const QuickViewInformation = (props) => {
  return ( 
    <div className='quick__view__modal'>
      <div className='bio__container flex-c'>
        <div class="bio__image__container">
          <img className='bio__image' src={require('../../assets/images/' + Personals.image_path)} alt='NOT LOADED' />
        </div>
        <span className='bio__name'>{Personals.name}</span>
        <span className='bio__description'>{Personals.bio}</span>
        <div className='flex-c bio__contact'>        
          <span className='bio__email'><i class="fa fa-envelope-o" />&nbsp;&nbsp;&nbsp;{Personals.email}</span>
          <span className='bio__phone'><i class="fa fa-phone" />&nbsp;&nbsp;&nbsp;{Personals.phone}</span>
        </div>
        <button className='bio__contact__button'>Add to contacts</button>
      </div>
      {LinksData && LinksData.map((linkGroup) => {
        return(
          <LinksContainer group={linkGroup} />
        );
      })}
    </div>
  );
}
 
export default QuickViewInformation;