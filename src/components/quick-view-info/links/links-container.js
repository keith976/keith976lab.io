import React from 'react';
import Links from './links';

//Styles
import './index.scss';

const LinksContainer = (props) => {
  const { group } = props;
  return (
    (group) && 
    <div className='link__group__container flex-c'>
      <span className='link__group__header'>{group.header}</span>
      {group.data.map((content) => {
        return(
          <Links LinkData={content} />
        );
      })}
    </div>
  );
}
 
export default LinksContainer;