import React from 'react';
const Links = (props) => {
  const { LinkData } = props;
  const linkContent = (
    <>
      <div className='icon__wrapper'>
        <img src={require(`../../../assets/icons/${LinkData.icon_name}`)} alt={LinkData.subheader} />
      </div>
      <div className='content flex-c'>
        <span className='link__individual__subheader'>{LinkData.subheader}</span>
        <span className='link__individual__content'>{LinkData.content}</span>
      </div>
    </>
  );

  return ( 
    <div className='link__individual__container'>
      <div className='link__individual__container--slide-animation flex-r'>
        {LinkData.link 
          ? <a href={LinkData.link} className='flex-r'>{linkContent}</a>
          : linkContent
        }
      </div>
    </div>  
  );
}
 
export default Links;