import React, { Component } from 'react';
import './App.css';

//Device Breakpoints
import Desktop from '../responsive/Desktop';
import Tablet from '../responsive/Tablet';
import Mobile from '../responsive/Mobile';

//Desktop View Modal
import DesktopView from './DesktopView/index';

//Quick View Modal
// import QuickViewModal from '../quick-view-info/index';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  render() { 
    return (
      <>
        <Desktop>
          <DesktopView />
        </Desktop>
        <Tablet>
          <DesktopView />
        </Tablet>
        <Mobile>
          <DesktopView />
        </Mobile>
      </>
    );
  }
}
export default App;