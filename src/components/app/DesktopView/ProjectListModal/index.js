import React, { useRef, useState } from 'react';
//Styles
import './index.scss';
import ProjectData from '@data/projects-data';

const ProjectListModal = (props) => {
  const tooltipBox = useRef(null);
  const tooltipImage = useRef(null);
  const [top, setTop] = useState(0);
  const [left, setLeft] = useState(0);
  const handleMouseOver = (image_src) => {
    tooltipBox.current.style.display = 'block';
    tooltipImage.current.src = require(`@assets/images/${image_src}`);
  }
  const handleMouseHoverOff = () => {
    tooltipBox.current.style.display = 'none';
    tooltipImage.current.src = 'null';
  }
  const handleMouseMove = (e) => {
    setLeft(e.pageX);
    setTop(e.pageY + 20);
  }
  return (
    <div className={`modal ${props.isModalEnabled ? 'modal__display' : ''}`} >
      <div className="modal__project__tooltip__image__container" ref={tooltipBox} style={{top: `${top}px`, left: `${left}px`}} >
        <img className="modal__project__tooltip__image" ref={tooltipImage} alt='project_image'/>
      </div>
      <div className='modal__header'>
        <div></div>
        <div className='modal__header__categories'>
          <button>All Types</button>
          <button>E-Commerce</button>
          <button>Art</button>
          <button>Government</button>
          <button>Services</button>
        </div>
        <button className='modal__header__close' onClick={() => props.handleModal()}>x</button>
      </div>
      <div className='modal__project__list'>
        <div className='modal__project__list__header'>
          <span></span>
          <span>Project</span>
          <span>Company</span>
          <span>Type</span>
          <span></span>
        </div>
        <div className='modal__project__list__content'>
          {
            ProjectData.map((project, i) => {
              return (
                <a href={`#${(i)}`} className='modal__project__list__content__link' onClick={() => props.handleModal()}>
                  <div className={`modal__project__list__content__data ${ProjectData.length > 7 ? 'modal__project__list__content__data-scroll' : ''}`}
                      onMouseEnter={() => handleMouseOver(project.project_image)}
                      onMouseLeave={handleMouseHoverOff}
                      onMouseMove={(e) => handleMouseMove(e)}
                  >
                    <span>{('0' + i).slice(-2)}</span>
                    <span>{project.project_title}</span>
                    <span>{project.project_company}</span>
                    <span>{project.project_type}</span>
                    <span className='project__card__data__header-last'>View</span>
                  </div>
                </a>
              )
            })
          }
        </div>
      </div>
    </div>
  );
}
export default ProjectListModal;