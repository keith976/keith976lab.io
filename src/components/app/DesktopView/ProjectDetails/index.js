import React from 'react';
//Styles
import './index.scss';

const ProjectDetails = (props) => {

  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <img src={require('@assets/images/' + props.project.project_image)} alt='header' style={{ maxWidth: '100%', border: '1px solid black', width: '75%'}}/>
      <div style={{ padding: '50px 280px', width: '100%', boxSizing: 'border-box' }}>
        <div style={{ display: 'grid', gridTemplateColumns: '1.6fr 1fr 1fr', gridAutoRows: 'minmax(100px, auto)', width: '100%' }}>
          <div style={{ gridColumn: '2', gridRow: '1', marginBottom: '100px' }}>
            <div className='description__container'>
              <span>Company</span>
              <span>{props.project.project_company}</span>
            </div>
            <div className='description__container'>
              <span>Title</span>
              <span>{props.project.project_title}</span>
            </div>
          </div>
          <div style={{ gridColumn: '3', gridRow: '1' }}>
            <div className='description__container'>
              <span>Type</span>
              <span>{props.project.project_type}</span>
            </div>
            <div className='description__container'>
              <span>Status</span>
              <span>{props.project.project_status}</span>
            </div>
          </div>
          <div style={{ gridColumn: '1', gridRow: '2' }}>
            <div className='metadata__container'>
              <div className='metadata__stack'>
                <div className='metadata__header'>Project Stack</div>
                <div className='metadata__content'>
                  <div className='metadata__content__entry left'>
                    {props.project.project_stack[0].map(entry => {
                      return (
                        <span className='metadata__content__text'>{entry}</span>
                      )
                    })}
                  </div>
                  <div className='metadata__content__entry right'>
                    {props.project.project_stack[1].map(entry => {
                      return (
                        <span className='metadata__content__text'>{entry}</span>
                      )
                    })}
                  </div>
                </div>
              </div>
              <div className='metadata__information'>
                <div className='metadata__header'>Additional Information</div>
                <div className='metadata__content'>
                  <div className='metadata__content__entry left'>
                    {props.project.project_information[0].map(entry => {
                      return (
                        <span className='metadata__content__text'>{entry}</span>
                      )
                    })}
                  </div>
                  <div className='metadata__content__entry right'>
                    {props.project.project_information[1].map(entry => {
                      return (
                        <span className='metadata__content__text'>{entry}</span>
                      )
                    })}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div style={{ gridColumn: '2/4', gridRow: '2' }}>{props.project.data[0]}</div>
        </div>
      </div>
    </div>
  );
}
export default ProjectDetails;