import React, { useRef, useEffect } from 'react';
//Styles
import './index.scss';
import ProjectData from '@data/projects-data';
import {
  NavLink
} from "react-router-dom";

const ProjectCard = (props) => {
  const inputRef = useRef([]);
  const handleScroll = () => {
    let screenSize = window.innerHeight;
    for(let i = 0; i < ProjectData.length; i++){
      if (window.scrollY > (inputRef.current[i].offsetTop + (screenSize))) {
        inputRef.current[i].classList.remove('project__card__wrapper__link__fade');
        inputRef.current[i].classList.add('project__card__wrapper__link__disappear');
      } else if (window.scrollY > (inputRef.current[i].offsetTop + (screenSize / 2))) {
        inputRef.current[i].classList.remove('project__card__wrapper__link__disappear');
        inputRef.current[i].classList.add('project__card__wrapper__link__fade');
      } else {
        inputRef.current[i].classList.remove('project__card__wrapper__link__disappear');
        inputRef.current[i].classList.remove('project__card__wrapper__link__fade');
      }
    }
  }
  
  useEffect(() => {
    inputRef.current = new Array(ProjectData.length);
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };

  }, []);

  return (
    <>
      {
        ProjectData.map((project, index) => {
          return (
            <NavLink to={project.project_path} className='project__card__wrapper__link' ref={el=>inputRef.current[index]=el}>
              <div className='project__card__wrapper' id={index}>
                <div className='project__card__data__wrapper'>
                  <div className='project__card__data__header'>
                    <span>{('0' + index).slice(-2)}</span>
                    <span>{project.project_title}</span>
                    <span>{project.project_company}</span>
                    <span>{project.project_type}</span>
                    <span className='project__card__data__header-last'>View</span>
                  </div>
                  <div className='project__card__data__image__wrapper'>
                    <img src={require('@assets/images/' + project.project_image)} alt='test' />
                  </div>
                </div>
              </div>
            </NavLink>
          );
        })
      }
    </>
  );
}
export default ProjectCard;