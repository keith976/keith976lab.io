import React from 'react';
//Styles
import './index.scss';

import ProjectCard from './ProjectCard';
import AnimatedHeaderText from './AnimatedHeaderText';
import ProjectListModal from './ProjectListModal';
import ProjectData from '@data/projects-data';
import ProjectDetails from './ProjectDetails';
import Scrollchor from 'react-scrollchor';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink
} from "react-router-dom";

class DesktopView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      viewHeight: 100,
      opacity: 1,
      isModalEnabled: false,
    }
    this.handleScroll = this.handleScroll.bind(this);
  }
  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }
  handleScroll = () => {
    this.setState({ viewHeight: 100 - ((window.scrollY / window.innerHeight) * 100) })
    this.setState({ opacity: 1 - ((window.scrollY / window.innerHeight) * 1.2) })
  }

  handleModal = () => {
    this.setState({ isModalEnabled: !this.state.isModalEnabled });
  }

  render() {
    return (
      <Router>
        <div className={`desktop__container ${this.state.isModalEnabled ? 'desktop__hidden' : ''}`}>
          <div className='navbar__container'>
            <div id='navbar__first__wrapper'>
              <NavLink to='/'>Keith Eng</NavLink>
            </div>
            <a href='#0' onClick={() => this.handleModal()}>Projects</a>
            <a href='#0'>Journal</a>
            <a href='#0'>About</a>
            <a href='#0' id='navbar__last__link'>Contact</a>
          </div>
          <Switch>
            <Route exact path='/'>
              <div className='header__wrapper'>
                <div className='header__container' style={{ height: `${ this.state.viewHeight }vh`, opacity: `${ this.state.opacity }` }}>
                  <Scrollchor to='#0' style={{ textDecoration: 'none' }}>
                    <h1 className='header__text header__text-first'>Eng Zhen Xuan, Keith</h1>
                    <AnimatedHeaderText className='header__text' />
                  </Scrollchor>
                </div>
              </div>
              <ProjectCard />
            </Route>
            { 
              ProjectData.map((project, index) => {
                return (
                  <Route exact path={project.project_path}>
                    <ProjectDetails project={project} index={index} />
                  </Route>
                )
              })
            }
          </Switch>
        </div>
        <ProjectListModal handleModal={this.handleModal} isModalEnabled={this.state.isModalEnabled} />
      </Router>
    );
  }
}
export default DesktopView;