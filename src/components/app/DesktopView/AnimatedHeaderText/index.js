import React, { useEffect, useState } from "react";

const header_titles = [
  "Developer",
  "Innovationist",
  "Designer",
  "Solutionist"
];
 
const AnimatedHeaderText = (props) => {
  const [index, setIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setIndex(index => index + 1);
    }, 3000);
    return () => clearInterval(interval);
  }, []);
 
  return (
    <h1 className={props.className}>
      {header_titles[index % header_titles.length]}
    </h1>
  );
};

export default AnimatedHeaderText;