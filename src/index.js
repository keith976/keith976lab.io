import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app/App';

import './assets/scss/common.scss';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);